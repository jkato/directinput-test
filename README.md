# DirectInput Test

Simple application that display some information about the first DirectInput device found. This is heavily based on the DirectInput example provided in the DirectX SDK.

This is only designed to run on Windows due to the DirectX and DirectInput dependencies.
