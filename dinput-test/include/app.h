#pragma once
#include <string>
#include <vector>

class App
{
public:
    App() = default;
    ~App() = default;

    int Run(const std::vector<std::string>& args);

private:
    bool InitializeCOM();
};
