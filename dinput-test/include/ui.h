#pragma once
#include <dinput_info.h>

class UserInterface
{
    DirectInputInfo* dInputInfo_ = nullptr;
    bool* pRun_ = nullptr;

    static void DrawDirectInputState(const InputState* state);

public:
    UserInterface() = default;
    ~UserInterface() = default;

    void Draw();

    void SetDirectInputInfo(DirectInputInfo* info);
    void SetRunPointer(bool* pValue);

private:
    void DrawControllerSelection() const;

    void DrawDirectInputInfoContent() const;
    void DrawDirectInputInfoWindow();
};