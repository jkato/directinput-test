#pragma once
#include <cstdint>
#include <string>
#include <vector>
#include <platform.h>

struct InputState
{
    bool XAxisEnabled;
    bool YAxisEnabled;
    bool ZAxisEnabled;

    bool XRotateEnabled;
    bool YRotateEnabled;
    bool ZRotateEnabled;

    bool Slider0Enabled;
    bool Slider1Enabled;

    bool POV0Enabled;
    bool POV1Enabled;
    bool POV2Enabled;
    bool POV3Enabled;

    int32_t SliderCount = 0;
    int32_t POVCount = 0;

    int32_t XAxis = 0;
    int32_t YAxis = 0;
    int32_t ZAxis = 0;

    int32_t XRotation = 0;
    int32_t YRotation = 0;
    int32_t ZRotation = 0;

    int32_t Slider0 = 0;
    int32_t Slider1 = 0;

    uint32_t POV0 = 0;
    uint32_t POV1 = 0;
    uint32_t POV2 = 0;
    uint32_t POV3 = 0;

    int32_t ButtonState[128];
};

struct DirectInputController
{
    GUID Instance;
    GUID Product;

    std::string InstanceGuid;
    std::string ProductGuid;

    std::string InstanceName;
    std::string ProductName;

    LPDIRECTINPUTDEVICE8 Device = nullptr;

    InputState State = { };
};

class DirectInputInfo
{
    std::vector<DirectInputController> controllers_;

    LPDIRECTINPUT8 dInput_ = nullptr;

    DIJOYCONFIG preferredController_;
    DirectInputController* selectedController_ = nullptr;

    HWND window_;

public:
    DirectInputInfo(HWND window);
    ~DirectInputInfo();

    std::vector<DirectInputController>* GetControllers();
    [[nodiscard]] DirectInputController* GetSelectedController() const;

    HRESULT InitializeDirectInput();

    HRESULT ReloadDevices();

    void SetSelectedController(DirectInputController* controller);

    void UpdateControllerState(DirectInputController* controller);
};
