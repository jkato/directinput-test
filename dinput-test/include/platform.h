#pragma once
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
#include <dinput.h>
#include <dinputd.h>
#include <d3d11.h>
#include <wrl/client.h>

bool disable_virtual_terminal();
bool enable_virtual_terminal();