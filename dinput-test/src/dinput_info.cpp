#include <iostream>
#include <string>
#include <vector>
#include <dinput_info.h>

using Microsoft::WRL::ComPtr;

#define SAFE_RELEASE(ptr) { if (ptr) { (ptr)->Release(); (ptr)=nullptr; } }

std::string guid_to_string(const GUID& id)
{
    wchar_t str[40] = { 0 };
    auto _ = StringFromGUID2(id, str, 40);

    char strId[40] = { 0 };
    WideCharToMultiByte(CP_ACP, 0, str, -1, strId, 40, nullptr, nullptr);
    return std::string(strId);
}


BOOL CALLBACK EnumDirectInputControllers(const DIDEVICEINSTANCE* pInstance, VOID* pContext)
{
    if (pInstance == nullptr)
        return DIENUM_STOP;

    const auto guidInstance = pInstance->guidInstance;
    const auto guidProduct = pInstance->guidProduct;
    const auto wInstanceName = std::wstring(pInstance->tszInstanceName);
    const auto wProductName = std::wstring(pInstance->tszProductName);

    auto* controllers = static_cast<std::vector<DirectInputController>*>(pContext);
    controllers->emplace_back(DirectInputController
        {
            .Instance = pInstance->guidInstance,
            .Product = pInstance->guidProduct,
            .InstanceGuid = guid_to_string(guidInstance),
            .ProductGuid = guid_to_string(guidProduct),
            .InstanceName = std::string(wInstanceName.begin(), wInstanceName.end()),
            .ProductName = std::string(wProductName.begin(), wProductName.end()),
        });

    return DIENUM_CONTINUE;
}

BOOL CALLBACK EnumControllerObjects(const DIDEVICEOBJECTINSTANCE* pObjInstance, VOID* context)
{
    if (pObjInstance == nullptr)
        return DIENUM_STOP;

    auto* controller = static_cast<DirectInputController*>(context);
    if (controller == nullptr)
        return DIENUM_STOP;

    if (pObjInstance->dwType & DIDFT_AXIS)
    {
        DIPROPRANGE range;
        range.diph.dwSize = sizeof DIPROPRANGE;
        range.diph.dwHeaderSize = sizeof DIPROPHEADER;
        range.diph.dwHow = DIPH_BYID;
        range.diph.dwObj = pObjInstance->dwType;
        range.lMin = -1000;
        range.lMax = 1000;

        const auto result = controller->Device->SetProperty(DIPROP_RANGE, &range.diph);
        if (FAILED(result))
            return DIENUM_STOP;
    }

    // Axises
    if (pObjInstance->guidType == GUID_XAxis)
        controller->State.XAxisEnabled = true;

    if (pObjInstance->guidType == GUID_YAxis)
        controller->State.YAxisEnabled = true;

    if (pObjInstance->guidType == GUID_ZAxis)
        controller->State.ZAxisEnabled = true;

    // Rotation Axises
    if (pObjInstance->guidType == GUID_RxAxis)
        controller->State.XRotateEnabled = true;

    if (pObjInstance->guidType == GUID_RyAxis)
        controller->State.YRotateEnabled = true;

    if (pObjInstance->guidType == GUID_RzAxis)
        controller->State.ZRotateEnabled = true;

    // Sliders
    if (pObjInstance->guidType == GUID_Slider)
    {
        switch (controller->State.SliderCount++)
        {
        case 0:
            controller->State.Slider0Enabled = true;
            break;

        case 1:
            controller->State.Slider1Enabled = true;
            break;

        default:
            break;
        }
    }

    // POV Hat(s)
    if (pObjInstance->guidType == GUID_POV)
    {
        switch (controller->State.POVCount++)
        {
        case 0:
            controller->State.POV0Enabled = true;
            break;

        case 1:
            controller->State.POV1Enabled = true;
            break;

        case 2:
            controller->State.POV2Enabled = true;
            break;

        case 3:
            controller->State.POV3Enabled = true;
            break;

        default:
            break;
        }
    }

    return DIENUM_CONTINUE;
}

DirectInputInfo::DirectInputInfo(HWND window) :
    preferredController_({}), window_(window)
{
    preferredController_.dwSize = sizeof preferredController_;
}

DirectInputInfo::~DirectInputInfo()
{
    for (auto& controller : controllers_)
    {
        SAFE_RELEASE(controller.Device)
    }

    SAFE_RELEASE(dInput_)
}


std::vector<DirectInputController>* DirectInputInfo::GetControllers()
{
    return &controllers_;
}

DirectInputController* DirectInputInfo::GetSelectedController() const
{
    return selectedController_;
}


HRESULT DirectInputInfo::InitializeDirectInput()
{
    // We only want to initialize DirectInput once.
    if (dInput_ != nullptr)
        return S_OK;

    const auto result = DirectInput8Create(
        GetModuleHandle(nullptr),
        DIRECTINPUT_VERSION,
        IID_IDirectInput8,
        reinterpret_cast<void**>(&dInput_),
        nullptr);

    if (FAILED(result))
        return result;

    return ReloadDevices();
}

void DirectInputInfo::SetSelectedController(DirectInputController* controller)
{
    selectedController_ = controller;
}

HRESULT DirectInputInfo::ReloadDevices()
{
    // Free and release any devices we may already have.
    if (!controllers_.empty())
    {
        for (auto& controller : controllers_)
        {
            SAFE_RELEASE(controller.Device)
        }

        controllers_.clear();
    }

    ComPtr<IDirectInputJoyConfig8> pJoyConfig;
    auto result = dInput_->QueryInterface(
        IID_IDirectInputJoyConfig8,
        &pJoyConfig);

    if (FAILED(result))
        return result;

    result = pJoyConfig->GetConfig(0, &preferredController_, DIJC_GUIDINSTANCE);
    if (FAILED(result))
        return result;

    result = dInput_->EnumDevices(
        DI8DEVCLASS_GAMECTRL,
        EnumDirectInputControllers,
        &controllers_,
        DIEDFL_ATTACHEDONLY);

    if (FAILED(result))
        return result;

    // Ensure we actually got some devices.
    // If we didn't actually find anything it won't cause too many issues,
    // there just won't be any devices shown to the user.
    if (controllers_.empty())
        return S_OK;

    for (auto& controller : controllers_)
    {
        result = dInput_->CreateDevice(controller.Instance, &controller.Device, nullptr);
        if (FAILED(result))
        {
#ifdef _DEBUG
            printf("[\x1b[1;31mFAILED\x1b[0m] Failed to create DirectInput device for %s\n", controller.InstanceGuid.c_str());
#endif
            continue;
        }
#ifdef _DEBUG
        printf("[\x1b[1;32mOK    \x1b[0m] Created device %s\n", controller.InstanceGuid.c_str());
#endif

        result = controller.Device->SetDataFormat(&c_dfDIJoystick2);
        if (FAILED(result))
            continue;

        result = controller.Device->SetCooperativeLevel(window_,
            DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);

        if (FAILED(result))
            continue;

        controller.Device->EnumObjects(
            EnumControllerObjects,
            &controller,
            DIDFT_ALL);

        if (IsEqualGUID(preferredController_.guidInstance, controller.Instance))
        {
            selectedController_ = &controller;
        }
    }

    return S_OK;
}

void DirectInputInfo::UpdateControllerState(DirectInputController* controller)
{
    if (controller == nullptr || controller->Device == nullptr)
        return;

    auto result = controller->Device->Poll();
    if (FAILED(result))
    {
        result = controller->Device->Acquire();
        while (result == DIERR_INPUTLOST)
            result = controller->Device->Acquire();

        return;
    }

    // Get the controller's device state.
    DIJOYSTATE2 state;
    result = controller->Device->GetDeviceState(sizeof DIJOYSTATE2, &state);
    if (FAILED(result))
        return;

    auto* inputState = &(controller->State);

    // Axises
    inputState->XAxis = state.lX;
    inputState->YAxis = state.lY;
    inputState->ZAxis = state.lZ;

    // Rotation Axises
    inputState->XRotation = state.lRx;
    inputState->YRotation = state.lRy;
    inputState->ZRotation = state.lRz;

    // Sliders
    inputState->Slider0 = state.rglSlider[0];
    inputState->Slider1 = state.rglSlider[1];

    // POV
    inputState->POV0 = state.rgdwPOV[0];
    inputState->POV1 = state.rgdwPOV[1];
    inputState->POV2 = state.rgdwPOV[2];
    inputState->POV3 = state.rgdwPOV[3];

    // Button States
    for (auto idx = 0; idx < 128; idx++)
    {
        if (state.rgbButtons[idx] & 0x80)
            inputState->ButtonState[idx] = 1;
        else
            inputState->ButtonState[idx] = 0;
    }
}
