#include <platform.h>

enum class vt_mode
{
    disabled,
    enabled,
};

bool set_virtual_terminal_mode(const vt_mode& mode)
{
#ifndef _WIN32
    return true;
#endif

    auto hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
    if (hStdOut == INVALID_HANDLE_VALUE)
        return false;

    DWORD outputMode = 0;
    GetConsoleMode(hStdOut, &outputMode);
    switch (mode)
    {
    case vt_mode::disabled:
        if ((outputMode & ENABLE_VIRTUAL_TERMINAL_PROCESSING) != 0)
            SetConsoleMode(hStdOut, outputMode & ~ENABLE_VIRTUAL_TERMINAL_PROCESSING);
        break;
    case vt_mode::enabled:
        if ((outputMode & ENABLE_VIRTUAL_TERMINAL_PROCESSING) == 0)
            SetConsoleMode(hStdOut, outputMode | ENABLE_VIRTUAL_TERMINAL_PROCESSING);
        break;
    default:
        return false;
    }

    return true;
}

bool disable_virtual_terminal()
{
    return set_virtual_terminal_mode(vt_mode::disabled);
}

bool enable_virtual_terminal()
{
    return set_virtual_terminal_mode(vt_mode::enabled);
}

