#include <string>
#include <memory>
#include <vector>
#include <app.h>
#ifndef _DEBUG
#include <platform.h>
#endif

#ifdef _DEBUG
int main(int argc, char* argv[])
{
    std::vector<std::string> args;
    args.reserve(argc);
    for (auto idx = 0; idx < argc; idx++)
    {
        args.emplace_back(std::string(argv[idx]));
    }

    const auto app = std::make_unique<App>();
    return app->Run(args);
}
#else
INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR lpCommandLine, INT nCmdShow)
{
    const auto args = std::vector<std::string>();

    const auto app = std::make_unique<App>();
    return app->Run(args);
}
#endif