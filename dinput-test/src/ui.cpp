#include <ui.h>
#include <dinput_info.h>
#include "imgui/imgui.h"

void UserInterface::DrawDirectInputState(const InputState* state)
{
    if (state == nullptr)
        return;

    ImGui::Text("Axises");
    ImGui::Separator();

    if (state->XAxisEnabled)
        ImGui::LabelText("X Axis", "%d", state->XAxis);

    if (state->YAxisEnabled)
        ImGui::LabelText("Y Axis", "%d", state->YAxis);

    if (state->ZAxisEnabled)
        ImGui::LabelText("Z Axis", "%d", state->ZAxis);

    ImGui::Text("Rotation");
    ImGui::Separator();

    if (state->XRotateEnabled)
        ImGui::LabelText("X Rotation", "%d", state->XRotation);

    if (state->YRotateEnabled)
        ImGui::LabelText("Y Rotation", "%d", state->YRotation);

    if (state->ZRotateEnabled)
        ImGui::LabelText("Z Rotation", "%d", state->ZRotation);

    if (state->SliderCount > 0)
    {
        ImGui::Text("Sliders");
        ImGui::Separator();
        if (state->Slider0Enabled)
            ImGui::LabelText("Slider 0", "%d", state->Slider0);

        if (state->Slider1Enabled)
            ImGui::LabelText("Slider 1", "%d", state->Slider1);
    }

    if (state->POVCount > 0)
    {
        ImGui::Text("POV");
        ImGui::Separator();
        if (state->POV0Enabled)
            ImGui::LabelText("POV0", "%d", state->POV0);

        if (state->POV1Enabled)
            ImGui::LabelText("POV1", "%d", state->POV1);

        if (state->POV2Enabled)
            ImGui::LabelText("POV2", "%d", state->POV2);

        if (state->POV3Enabled)
            ImGui::LabelText("POV3", "%d", state->POV3);
    }

    ImGui::Text("Buttons");
    ImGui::Separator();
    for (auto idx = 0; idx < 128; idx++)
    {
        if (state->ButtonState[idx] != 0)
            ImGui::Text("Button %03d", idx);
    }
}

void UserInterface::Draw()
{
    DrawDirectInputInfoWindow();
}

void UserInterface::SetDirectInputInfo(DirectInputInfo* info)
{
    dInputInfo_ = info;
}

void UserInterface::SetRunPointer(bool* pValue)
{
    pRun_ = pValue;
}

void UserInterface::DrawControllerSelection() const
{
    const char* selectedName = "";
    auto* selectedController = dInputInfo_->GetSelectedController();
    if (selectedController != nullptr)
    {
        selectedName = selectedController->ProductName.c_str();
    }

    auto* controllers = dInputInfo_->GetControllers();
    if (controllers->empty())
    {
        ImGui::Text("No DirectInput devices found!");
        return;
    }

    if (ImGui::BeginCombo("Controllers", selectedName))
    {
        for (auto& controller : *controllers)
        {
            const auto name = controller.ProductName.c_str();
            const auto isSelected = selectedName == name;
            if (ImGui::Selectable(name, isSelected))
            {
                dInputInfo_->SetSelectedController(&controller);
            }
        }

        ImGui::EndCombo();
    }
}


void UserInterface::DrawDirectInputInfoContent() const
{
    DrawControllerSelection();
    ImGui::Separator();

    auto* controller = dInputInfo_->GetSelectedController();
    if (controller == nullptr)
    {
        ImGui::Text("No controller selected!");
        return;
    }

    ImGui::Text("%s", controller->InstanceGuid.c_str());
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::Text("Instance ID");
        ImGui::EndTooltip();
    }
    ImGui::Separator();

    // Perform the update for the controller state before
    // we display information about the controller.
    dInputInfo_->UpdateControllerState(controller);

    // Draw the actual window content.
    DrawDirectInputState(&controller->State);
}

void UserInterface::DrawDirectInputInfoWindow()
{
    static constexpr auto windowStyle =
        ImGuiWindowFlags_NoMove |
        ImGuiWindowFlags_NoCollapse |
        ImGuiWindowFlags_NoTitleBar |
        ImGuiWindowFlags_NoResize |
        ImGuiWindowFlags_NoBringToFrontOnFocus |
        ImGuiWindowFlags_MenuBar |
        ImGuiWindowFlags_NoDocking;

    const auto* viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos(viewport->Pos);
    ImGui::SetNextWindowSize(viewport->Size);

    if (!ImGui::Begin("DirectInput Info", nullptr, windowStyle))
        return;

    if (ImGui::BeginMenuBar())
    {
        if (ImGui::BeginMenu("App"))
        {
            if (ImGui::MenuItem("Exit"))
                *pRun_ = false;

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("DirectInput"))
        {
            if (ImGui::MenuItem("Refresh Devices"))
                dInputInfo_->ReloadDevices();

            ImGui::EndMenu();
        }

        ImGui::EndMenuBar();
    }

    // We don't really want to display anything in the UI
    // if we don't have any DirectInput information to actually show.
    if (dInputInfo_ == nullptr || dInputInfo_->GetControllers()->empty())
        ImGui::Text("No DirectInput devices available.");
    else
        DrawDirectInputInfoContent();

    ImGui::End();
}
