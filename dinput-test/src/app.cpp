#include <app.h>
#include <platform.h>
#include <render_window.h>

int App::Run(const std::vector<std::string>& args)
{
    enable_virtual_terminal();

    if (!InitializeCOM())
        return E_FAIL;

    ShowRenderWindow(args);

    disable_virtual_terminal();

    CoUninitialize();
    return 0;
}

bool App::InitializeCOM()
{
    if (FAILED(CoInitialize(nullptr)))
    {
#ifdef _DEBUG
        printf("[\x1b[1;31mFAILED\x1b[0m] Failed to initialize COM. Application will exit.\n");
#else
        MessageBox(nullptr,
            L"Failed to initialize COM. Application will exit.",
            L"dinput-test",
            MB_OK | MB_ICONERROR);
#endif
    }
#ifdef _DEBUG
    else
    {
        printf("[\x1b[1;32mOK    \x1b[0m] COM initialized.\n");
    }
#endif

    return true;
}
